package com.letgo.scala_candidate_test.service

import com.github.blemale.scaffeine.{AsyncLoadingCache, Scaffeine}
import org.mockito.Mockito._
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.mockito.MockitoSugar
import scala.concurrent.Future

import com.letgo.scala_candidate_test.Utils._
import com.letgo.scala_candidate_test.domain.Tweet

class ShoutServiceTest extends FlatSpec with Matchers with MockitoSugar {

  "shoutTweet" should "return tweets in uppercase and with an exclamation at the end." in new Context {
    when(cacheService.tweetByUserNameCache).thenReturn(mockCache)

    awaitForResult(shoutService.shoutTweets("username", 1)) shouldBe Seq("FOO!")
    awaitForResult(shoutService.shoutTweets("username", 3)) shouldBe Seq("FOO!", "BAR!", "HELLO, I'M MARTIN!")
    awaitForResult(shoutService.shoutTweets("username", 2)) shouldBe Seq("FOO!", "BAR!")
  }

  trait Context {
    lazy val mockCache: AsyncLoadingCache[cacheService.TweetCacheKey, Future[Seq[Tweet]]] = {
      val tweets: Seq[Tweet] = Seq(Tweet("foo"), Tweet("bar!"), Tweet("Hello, I'm Martin"))
      Scaffeine()
        .recordStats()
        .buildAsync(_ => Future.successful(tweets), None, None)
    }

    val cacheService: CacheService = mock[CacheService]
    val shoutService: ShoutService = new ShoutService(cacheService)
  }
}
