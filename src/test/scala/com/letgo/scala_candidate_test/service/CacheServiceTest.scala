package com.letgo.scala_candidate_test.service

import org.scalatest.{FlatSpec, Matchers}

import com.letgo.scala_candidate_test.Utils._
import com.letgo.scala_candidate_test.domain.Tweet
import com.letgo.scala_candidate_test.infrastructure.TweetRepositoryInMemory

class CacheServiceTest extends FlatSpec with Matchers {

  "tweetByUserNameCache" should "return the same result for a username if the time has not expired" in new Context {
    val key = cacheService.TweetCacheKey("username")
    val result1: Seq[Tweet] =
      awaitForResult(cacheService.tweetByUserNameCache.get(key).flatten)

    val result2: Seq[Tweet] =
      awaitForResult(cacheService.tweetByUserNameCache.get(key).flatten)

    result1 shouldBe result2
  }

  trait Context {
    val tweetRepositoryInMemory    = new TweetRepositoryInMemory()
    val cacheService: CacheService = new CacheService(tweetRepositoryInMemory)
  }
}
