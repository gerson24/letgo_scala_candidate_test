package com.letgo.scala_candidate_test.application

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import org.mockito.Mockito._
import org.scalatest.{Matchers, WordSpec}
import org.scalatest.mockito.MockitoSugar
import scala.concurrent.Future

import com.letgo.scala_candidate_test.Constants._
import com.letgo.scala_candidate_test.error.ApiError
import com.letgo.scala_candidate_test.service.ShoutService

class ShoutControllerTest
    extends WordSpec
    with Matchers
    with ScalatestRouteTest
    with MockitoSugar
    with FailFastCirceSupport {

  "ShoutController" when {

    "GET /should/username?limit=2" should {

      "return 200 and a list with 2 tweets shouting" in new Context {
        when(shoutService.shoutTweets("username", 2))
          .thenReturn(Future.successful(Seq("TWEET1!", "TWEET2!")))

        Get("/shout/username?limit=2") ~> route ~> check {
          responseAs[Seq[String]] shouldBe Seq("TWEET1!", "TWEET2!")
          status.intValue() shouldEqual 200
        }
      }

      "return 500 if something wrong was happened" in new Context {
        when(shoutService.shoutTweets("username", 2))
          .thenThrow(new RuntimeException("something wrong"))

        Get("/shout/username?limit=2") ~> route ~> check {
          responseAs[ApiError] shouldBe ApiError(UNEXPECTED_API_ERROR, "something wrong")
          status.intValue() shouldEqual 500
        }
      }
    }

    "GET /should/username" should {

      "return 400 if limit parameter is missing" in new Context {

        Get("/shout/username") ~> route ~> check {
          responseAs[ApiError] shouldBe ApiError(MANDATORY_PARAMETER_API_ERROR, "limit is mandatory")
          status.intValue() shouldEqual 400
        }
      }
    }

    "GET /should/username?limit=100" should {

      "return 400 if limit is greater than 10" in new Context {

        Get("/shout/username?limit=100") ~> route ~> check {
          responseAs[ApiError] shouldBe ApiError(
            PARAMETER_VALIDATION_API_ERROR,
            s"Limit must be equal or less than $MAX_LIMIT"
          )
          status.intValue() shouldEqual 400
        }
      }
    }

    "GET /notFound" should {

      "return 404 if requested resource does not exist" in new Context {

        Get("/notFound") ~> route ~> check {
          responseAs[ApiError] shouldBe ApiError(
            NOT_FOUND_API_ERROR,
            "The requested resource could not be found."
          )
          status.intValue() shouldEqual 404
        }
      }
    }
  }

  trait Context {
    val shoutService: ShoutService       = mock[ShoutService]
    val shoutController: ShoutController = new ShoutController(shoutService)

    val route: Route = shoutController.route
  }
}
