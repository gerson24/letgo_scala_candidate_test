package com.letgo.scala_candidate_test

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._

object Utils {
  implicit val actorSystem: ActorSystem           = ActorSystem("test")
  implicit val materializer: ActorMaterializer    = ActorMaterializer()(actorSystem)
  implicit val executionContext: ExecutionContext = materializer.executionContext

  def awaitForResult[T](futureResult: Future[T]): T =
    Await.result(futureResult, 5.seconds)
}
