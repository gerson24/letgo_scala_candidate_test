package com.letgo.scala_candidate_test.application

import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.{ExceptionHandler, MissingQueryParamRejection, RejectionHandler, Route, ValidationRejection}
import akka.http.scaladsl.server.Directives._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._

import com.letgo.scala_candidate_test.Constants._
import com.letgo.scala_candidate_test.error.ApiError
import com.letgo.scala_candidate_test.service.ShoutService

class ShoutController(shoutService: ShoutService) extends FailFastCirceSupport {

  private val errorHandler = ExceptionHandler {
    case err => complete(InternalServerError -> ApiError(UNEXPECTED_API_ERROR, err.getMessage))
  }

  private val rejectHandler = RejectionHandler
    .newBuilder()
    .handle {
      case MissingQueryParamRejection(param) =>
        complete(BadRequest -> ApiError(MANDATORY_PARAMETER_API_ERROR, s"$param is mandatory"))
      case ValidationRejection(message, _) =>
        complete(BadRequest -> ApiError(PARAMETER_VALIDATION_API_ERROR, message))
      case _ => complete(InternalServerError -> "Unexpected rejection")
    }
    .handleNotFound(
      complete(NotFound -> ApiError(NOT_FOUND_API_ERROR, "The requested resource could not be found."))
    )
    .result()

  val route: Route = get {
    handleRejections(rejectHandler) {
      handleExceptions(errorHandler) {
        path("shout" / Segment) { twitterUserName =>
          parameters('limit.as[Int]) { limit =>
            validate(limit <= MAX_LIMIT, s"Limit must be equal or less than $MAX_LIMIT") {
              complete(shoutService.shoutTweets(twitterUserName, limit))
            }
          }
        }
      }
    }
  }
}
