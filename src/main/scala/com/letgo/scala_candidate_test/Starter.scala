package com.letgo.scala_candidate_test

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import scala.concurrent.ExecutionContext

import com.letgo.scala_candidate_test.application.ShoutController
import com.letgo.scala_candidate_test.infrastructure.TweetRepositoryInMemory
import com.letgo.scala_candidate_test.service.{CacheService, ShoutService}

object Starter {
  def main(args: Array[String]): Unit = {

    implicit val actorSystem: ActorSystem           = ActorSystem()
    implicit val materializer: ActorMaterializer    = ActorMaterializer()(actorSystem)
    implicit val executionContext: ExecutionContext = materializer.executionContext
    implicit lazy val log: LoggingAdapter           = Logging(actorSystem, this.getClass)

    val tweetRepositoryInMemory = new TweetRepositoryInMemory()
    val cacheService            = new CacheService(tweetRepositoryInMemory)
    val shoutService            = new ShoutService(cacheService)
    val shoutController         = new ShoutController(shoutService)

    Http().bindAndHandle(shoutController.route, "0.0.0.0", 9000)
  }
}
