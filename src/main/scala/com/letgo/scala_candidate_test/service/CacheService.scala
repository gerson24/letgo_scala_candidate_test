package com.letgo.scala_candidate_test.service

import com.github.blemale.scaffeine.{AsyncLoadingCache, Scaffeine}
import scala.concurrent.Future
import scala.concurrent.duration._

import com.letgo.scala_candidate_test.Constants.MAX_LIMIT
import com.letgo.scala_candidate_test.domain.Tweet
import com.letgo.scala_candidate_test.infrastructure.TweetRepositoryInMemory

class CacheService(tweetRepositoryInMemory: TweetRepositoryInMemory) {
  import tweetRepositoryInMemory.searchByUserName

  case class TweetCacheKey(username: String)

  lazy val tweetByUserNameCache: AsyncLoadingCache[TweetCacheKey, Future[Seq[Tweet]]] =
    Scaffeine()
      .recordStats()
      .expireAfterAccess(5.minutes)
      .buildAsync(
        key => searchByUserName(key.username, MAX_LIMIT),
        None,
        None
      )
}
