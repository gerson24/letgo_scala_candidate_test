package com.letgo.scala_candidate_test.service

import scala.concurrent.{ExecutionContext, Future}

class ShoutService(cacheService: CacheService)(implicit executionContext: ExecutionContext) {
  import cacheService.{TweetCacheKey, tweetByUserNameCache}

  // I could use cats.IO instead of futures
  def shoutTweets(username: String, limit: Int): Future[Seq[String]] =
    tweetByUserNameCache.get(TweetCacheKey(username)).flatten.map(_.take(limit).map(_.toShout).map(_.text))
}
