package com.letgo.scala_candidate_test.error

sealed trait HttpError
case class ApiError(code: String, msg: String) extends HttpError
