package com.letgo.scala_candidate_test

object Constants {
  final val MAX_LIMIT = 10

  // Api errors
  final val PARAMETER_VALIDATION_API_ERROR = "API_ERROR_01"
  final val MANDATORY_PARAMETER_API_ERROR  = "API_ERROR_02"
  final val UNEXPECTED_API_ERROR           = "API_ERROR_03"
  final val NOT_FOUND_API_ERROR            = "API_ERROR_04"
}
