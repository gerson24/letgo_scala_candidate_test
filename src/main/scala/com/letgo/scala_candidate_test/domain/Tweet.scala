package com.letgo.scala_candidate_test.domain

case class Tweet(text: String) {
  def toShout: Tweet =
    if (text.endsWith("!")) copy(text.toUpperCase) else copy(s"${text.toUpperCase}!")
}
