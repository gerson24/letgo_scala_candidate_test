name := "scala_candidate_test"

version := "0.1"

scalaVersion := "2.12.7"

scalacOptions += "-Ypartial-unification"

libraryDependencies ++= Seq(
  "com.typesafe.akka"  %% "akka-http"         % "10.1.5",
  "com.typesafe.akka"  %% "akka-actor"        % "2.5.4",
  "com.typesafe.akka"  %% "akka-stream"       % "2.5.4",
  "de.heikoseeberger"  %% "akka-http-circe"   % "1.22.0",
  "io.circe"           %% "circe-core"        % "0.11.1",
  "io.circe"           %% "circe-generic"     % "0.11.1",
  "io.circe"           %% "circe-parser"      % "0.11.1",
  "com.github.blemale" %% "scaffeine"         % "2.6.0" % Compile,
  "org.scalatest"      %% "scalatest"         % "3.0.5" % Test,
  "com.typesafe.akka"  %% "akka-http-testkit" % "10.1.5" % Test,
  "org.mockito"        % "mockito-all"        % "1.10.19" % Test
)

addCommandAlias("c", "compile")
addCommandAlias("s", "scalastyle")
addCommandAlias("tc", "test:compile")
addCommandAlias("ts", "test:scalastyle")
addCommandAlias("t", "test")
addCommandAlias("to", "testOnly")
